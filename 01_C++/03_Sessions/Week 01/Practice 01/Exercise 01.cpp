/**
 * @file Exercise 01.cpp
 * @author Juan Espinoza (juan.espinoza24@unmsm.com)
 * @brief Esta es el algoritmo para calcular el �rea lateral y el volumen de un cilindro.
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416      //N�mero PI

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	float CylinderRadius=0; 	//Radio de la base del cilindro
	float CylinderHeight=0; 	//Altura del cilindro
	float LateralArea=0; 		// �rea lateral del cilindro
	float CylinderVolume=0;		// Volumen del cilindro
	
	//Display phrase 1
	cout<<"==========INSERT DATA==========\r\n"
	cout<<"Inserte el radio de la base del cilindro:";
	cin>>CylinderRadius;
	
	//Display phrase 2
	cout<< "Inserte la altura del cilindro:";
	cin>>CylinderHeight;
	
	//Operations
	LateralArea = 2*3.14*CylinderRadius*CylinderHeight;
	CylinderVolume = 3.14*CylinderRadius*CylinderRadius*CylinderHeight;
	
	
	//Display phrase 3 with numbers
	cout<<"==========SHOW RESULTS==========\r\n"
	cout<<"El area lateral del cilindro es:"<<LateralArea;
	cout<<"\n\r El volumen del cilindro es:"<<CylinderVolume;
	
	
	
	return 0;
}

