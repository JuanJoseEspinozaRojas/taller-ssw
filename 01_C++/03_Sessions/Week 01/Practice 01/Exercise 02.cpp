/**
 * @file Exercise 01.cpp
 * @author Juan Espinoza (juan.espinoza24@unmsm.com)
 * @brief Esta es el algoritmo para calcular el porcentaje de hombres y mujeres en un grupo de estudiantes.
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	int InputNumH=0.0; 		//Cantidad de hombres en el grupo de estudiantes
	int InputNumM=0.0; 		//Cantidad de mujeres en el grupo de estudiantes
	int InputTotal=0.0; 	// Cantidad total de estudiantes
	float PerceMen=0.0; 	//Porcentaje de hombres
	float PerceWomen=0.0; 	//Porcentaje de mujeres
	
	//Display phrase 1
	cout<<"==========INSERT DATA==========\r\n"
	cout<<"Insertar la cantidad de hombres:";
	cin>>InputNumH;
	
	//Display phrase 2
	cout<<"Insertar la cantidad de mujeres:";
	cin>>InputNumM;
	
	//Operations
	InputTotal = InputNumH+InputNumM;
	PerceMen = (InputNumH*100)/InputTotal;
	PerceWomen = (InputNumM*100)/InputTotal;
	
	//Display phrase 3 with numbers
	cout<<"==========SHOW RESULTS==========\r\n"
	cout<<"El porcentaje de hombres es:"<<PerceMen<<"%";
	cout<<"\r\nEl porcentaje de mujeres es:"<<PerceWomen<<"%";
	
	return 0;
}
