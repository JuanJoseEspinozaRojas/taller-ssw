/**
 * @file Exercise 03.cpp
 * @author Juan Espinoza (juan.espinoza24@unmsm.com)
 * @brief Esta es el algoritmo para calcular el porcentaje total de estudiantes aprobados; % de estudiantes aprobados, desaprobados, notables y sobresalientes.
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main (){
	
	
	int DisapprovedStudent= 0;        	//Estudiantes desaprobados  <=10
	int ApprovedStudent= 0;        		//Estudiantes aprobados  11 al 14
	int SignStudent= 0;         		//Estudiantes notables  15 al 17
	int OutStudent= 0;        			//Estudiantes sobresalientes  18 al 20
	int TotalStudent= 0;        		//Total de estudiantes
	int PercentTotalApproved= 0;       	//Porcentaje total de estudiantes aprobados
	float PercentApproved =0.0;			//Porcentaje de aprobados
	float PercentDisapproved=0.0;  		//Porcentaje de desaprobados
	float PercentSign=0.0; 				//Porcentaje de notables
	float PercentOut=0.0;  				//Porcentaje de sobresalientes
	
	
	//Display of phrase 1
	cout<<"==========INSERT DATA==========\r\n"
	cout<<"Insertar cantidad de estudiantes aprobados:";
	cin>>ApprovedStudent;
	cout<<"Insertar cantidad de estudiantes desaprobados:";
	cin>>DisapprovedStudent;
	cout<<"Insertar cantidad de estudiantes notables:";
	cin>>SignStudent;
	cout<<"Insertar cantidad de estudiantes sobresalientes:";
	cin>>OutStudent;
	
	//Operations
	
	TotalStudent= ApprovedStudent+DisapprovedStudent+SignStudent+OutStudent;
	PercentTotalApproved= ((ApprovedStudent+SignStudent+OutStudent)*100)/TotalStudent;
	PercentApproved=(ApprovedStudent*100)/TotalStudent;
    PercentDisapproved=(DisapprovedStudent*100)/TotalStudent;
    PercentSign=(SignStudent*100)/TotalStudent;
    PercentOut=(SignStudent*100)/TotalStudent;
    
    
	//Display of phrase 2
	cout<<"==========SHOW RESULTS==========\r\n"
	cout<<"El porcentaje total de estudiantes aprobados es:"<<PercentTotalApproved<<"%";
	cout<<"\r\nEl porcentaje de estudiantes desaprobados es:"<<PercentDisapproved<<"%";
	cout<<"\r\nEl porcentaje de estudiantes aprobados es:"<<PercentApproved<<"%";
	cout<<"\r\nEl porcentaje de estudiantes notables es:"<<PercentSign<<"%";
	cout<<"\r\nEl porcentaje de estudiantes sobresalientes es:"<<PercentOut<<"%";
	
	return 0;
}

